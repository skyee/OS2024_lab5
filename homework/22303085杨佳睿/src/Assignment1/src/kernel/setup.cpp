#include "asm_utils.h"
#include "interrupt.h"
#include "stdio.h"

// 屏幕IO处理器
STDIO stdio;
// 中断管理器
InterruptManager interruptManager;


extern "C" void setup_kernel()
{
    // 中断处理部件
    interruptManager.initialize();
    // 屏幕IO处理部件
    stdio.initialize();
    interruptManager.enableTimeInterrupt();
    interruptManager.setTimeInterrupt((void *)asm_time_interrupt_handler);
    //asm_enable_interrupt();
    printf("print percentage: %%\n"
           "print char \"N\": %c\n"
           "print string \"I like os!\": %s\n"
           "print decimal: \"-1314520\": %d\n"
           "print hexadecimal \"0x7abcdef0\": %x\n",
           'Y', "I like os!", -1314520, 0x7abcdef0);
    //uint a = 1 / 0;
    asm_halt();
}
