<font size =6>**操作系统原理 实验五** `</font>`

## 个人信息

（此部分需补充完整）

【院系】计算机学院

【专业】计算机科学与技术

【学号】22303085

【姓名】杨佳睿

## 实验题目

内核线程

## 实验目的

1. 学习C语言的可变参数机制的实现方法，实现可变参数机制，以及实现一个较为简单的printf函数。
2. 实现内核线程的实现。
3. 重点理解 `asm_switch_thread`是如何实现线程切换的，体会操作系统实现并发执行的原理。
4. 实现基于时钟中断的时间片轮转(RR)调度算法，并实现一个线程调度算法。

## 实验要求

1. 实现了可变参数机制及printf函数。
2. 自行实现PCB，实现线程。
3. 了解线程调度，并实现一个调度算法。
4. 撰写实验报告。

## 实验方案

（此部分需补充完整）

### 1.实现printf函数

首先我们要掌握可变参数的实现，在掌握可变参数之后，printf的实现便不再困难。printf的作用是格式化输出，并返回输出的字符个数，其定义如下。

```
int printf(const char *const fmt, ...);
```

在格式化输出字符串中，会包含 `%c,%d,%x,%s`等来实现格式化输出，对应的参数在可变参数中可以找到。明白了printf的作用，printf的实现便迎刃而解，实现思路如下。

printf首先找到fmt中的形如 `%c,%d,%x,%s`对应的参数，然后用这些参数具体的值来替换 `%c,%d,%x,%s`等，得到一个新的格式化输出字符串，这个过程称为fmt的解析。最后，printf将这个新的格式化输出字符串即可。然而，这个字符串可能非常大，会超过函数调用栈的大小。实际上，我们会定义一个缓冲区，然后对fmt进行逐字符地解析，将结果逐字符的放到缓冲区中。放入一个字符后，我们会检查缓冲区，如果缓冲区已满，则将其输出，然后清空缓冲区，否则不做处理。

在实现printf前，我们需要一个能够输出字符串的函数，这个函数能够正确处理字符串中的 `\n`换行字符。这里，有同学会产生疑问，`\n`不是直接输出就可以了吗？其实 `\n`的换行效果是我们人为规定的，换行的实现需要我们把光标放到下一行的起始位置，如果光标超过了屏幕的表示范围，则需要滚屏。因此，我们实现一个能够输出字符串的函数 `STDIO::print`，声明和实现分别放在 `include/stdio.h`和 `src/kernel/stdio.cpp`中，如下所示。

```
int STDIO::print(const char *const str)
{
    int i = 0;

    for (i = 0; str[i]; ++i)
    {
        switch (str[i])
        {
        case '\n':
            uint row;
            row = getCursor() / 80;
            if (row == 24)
            {
                rollUp();
            }
            else
            {
                ++row;
            }
            moveCursor(row * 80);
            break;

        default:
            print(str[i]);
            break;
        }
    }

    return i;
}
```

接着按照思路最终printf实现如下：

```
int printf(const char *const fmt, ...)
{
    const int BUF_LEN = 32;

    char buffer[BUF_LEN + 1];
    char number[33];

    int idx, counter;
    va_list ap;

    va_start(ap, fmt);
    idx = 0;
    counter = 0;

    for (int i = 0; fmt[i]; ++i)
    {
        if (fmt[i] != '%')
        {
            counter += printf_add_to_buffer(buffer, fmt[i], idx, BUF_LEN);
        }
        else
        {
            i++;
            if (fmt[i] == '\0')
            {
                break;
            }

            switch (fmt[i])
            {
            case '%':
                counter += printf_add_to_buffer(buffer, fmt[i], idx, BUF_LEN);
                break;

            case 'c':
                counter += printf_add_to_buffer(buffer, va_arg(ap, int), idx, BUF_LEN);
                break;

            case 's':
                buffer[idx] = '\0';
                idx = 0;
                counter += stdio.print(buffer);
                counter += stdio.print(va_arg(ap, const char *));
                break;

            case 'd':
            case 'x':
                int temp = va_arg(ap, int);

                if (temp < 0 && fmt[i] == 'd')
                {
                    counter += printf_add_to_buffer(buffer, '-', idx, BUF_LEN);
                    temp = -temp;
                }

                temp = itos(number, temp, (fmt[i] == 'd' ? 10 : 16));

                for (int j = temp - 1; j >= 0; --j)
                {
                    counter += printf_add_to_buffer(buffer, number[j], idx, BUF_LEN);
                }
                break;

            }
        }
    }

    buffer[idx] = '\0';
    counter += stdio.print(buffer);

    return counter;
}
```

由于我们需要将要打印的字符输入到缓冲区中，因此我们使用函数 `printf_add_to_buffer`来实现：

```
int printf_add_to_buffer(char *buffer, char c, int &idx, const int BUF_LEN)
{
    int counter = 0;

    buffer[idx] = c;
    ++idx;

    if (idx == BUF_LEN)
    {
        buffer[idx] = '\0';
        counter = stdio.print(buffer);
        idx = 0;
    }

    return counter;
}
```

这个函数确保了当缓冲区慢的时候自动打印缓冲区的字符并清空缓冲区。

接下来我们测试这个函数，我们在 `setup_kernel`中加入对应的测试语句。

```
#include "asm_utils.h"
#include "interrupt.h"
#include "stdio.h"

// 屏幕IO处理器
STDIO stdio;
// 中断管理器
InterruptManager interruptManager;


extern "C" void setup_kernel()
{
    // 中断处理部件
    interruptManager.initialize();
    // 屏幕IO处理部件
    stdio.initialize();
    interruptManager.enableTimeInterrupt();
    interruptManager.setTimeInterrupt((void *)asm_time_interrupt_handler);
    //asm_enable_interrupt();
    printf("print percentage: %%\n"
           "print char \"N\": %c\n"
           "print string \"I like os!\": %s\n"
           "print decimal: \"-1314520\": %d\n"
           "print hexadecimal \"0x7abcdef0\": %x\n",
           'Y', "I like os!", -1314520, 0x7abcdef0);
    //uint a = 1 / 0;
    asm_halt();
}
```

### 2.设计PCB：

首先要明确我们要实现的线程状态有五个：分别是创建态、运行态、就绪态、阻塞态和终止态。我们使用一个枚举类型 `ProgramStatus`来描述线程的5个状态，代码放在 `include/thread.h`中。

```
enum ProgramStatus
{
    CREATED,
    RUNNING,
    READY,
    BLOCKED,
    DEAD
};
```

线程的组成部分线程各自的栈，状态，优先级，运行时间，线程负责运行的函数，函数的参数等，这些组成部分被集中保存在一个结构中——PCB(Process Control Block)，如下所示，代码放在 `include/thread.h`中。

```
struct PCB
{
    int *stack;                      // 栈指针，用于调度时保存esp
    char name[MAX_PROGRAM_NAME + 1]; // 线程名
    enum ProgramStatus status;       // 线程的状态
    int priority;                    // 线程优先级
    int pid;                         // 线程pid
    int ticks;                       // 线程时间片总时间
    int ticksPassedBy;               // 线程已执行时间
    ListItem tagInGeneralList;       // 线程队列标识
    ListItem tagInAllList;           // 线程队列标识
};
```

接着我们还需实现一个程序管理员，用来实现线程和进程的创建与管理

1.创建线程：首先要向内存中申请一个PCB大小的空间，然后创建线程

我们在 `ProgramManager`中声明两个管理PCB所在的内存空间函数。

```
// 分配一个PCB
PCB *allocatePCB();
// 归还一个PCB
void releasePCB(PCB *program);
```

`allocatePCB`会去检查 `PCB_SET`中每一个PCB的状态，如果找到一个未被分配的PCB，则返回这个PCB的起始地址。注意到 `PCB_SET`中的PCB是连续存放的，对于第**i**个PCB，`PCB_SET`的首地址加上**i**×**P**C**B**S**I**Z**E**就是第**i**个PCB的起始地址。PCB的状态保存在 `PCB_SET_STATUS`中，并且 `PCB_SET_STATUS`的每一项会在 `ProgramManager`总被初始化为 `false`，表示所有的PCB都未被分配。被分配的PCB用 `true`来标识。

如果 `PCB_SET_STATUS`的所有元素都是 `true`，表示所有的PCB都已经被分配，此时应该返回 `nullptr`，表示PCB分配失败。

allocatePCB函数实现如下：

```
PCB *ProgramManager::allocatePCB()
{
    for (int i = 0; i < MAX_PROGRAM_AMOUNT; ++i)
    {
        if (!PCB_SET_STATUS[i])
        {
            PCB_SET_STATUS[i] = true;
            return (PCB *)((int)PCB_SET + PCB_SIZE * i);
        }
    }

    return nullptr;
}
```

`releasePCB`接受一个PCB指针 `program`，然后计算出 `program`指向的PCB在 `PCB_SET`中的位置，然后将 `PCB_SET_STATUS`中的对应位置设置 `false`即可。

releasePCB函数实现如下：

```
void ProgramManager::releasePCB(PCB *program)
{
    int index = ((int)program - (int)PCB_SET) / PCB_SIZE;
    PCB_SET_STATUS[index] = false;
}
```

另外，在使用 `ProgramManager`的成员函数前，我们必须初始化 `ProgramManager`

用函数initialize实现：

```
void ProgramManager::initialize()
{
    allPrograms.initialize();
    readyPrograms.initialize();
    running = nullptr;

    for (int i = 0; i < MAX_PROGRAM_AMOUNT; ++i)
    {
        PCB_SET_STATUS[i] = false;
    }
}
```

还需要创建线程的函数 `executeThread `：

```
int ProgramManager::executeThread(ThreadFunction function, void *parameter, const char *name, int priority)
{
    // 关中断，防止创建线程的过程被打断
    bool status = interruptManager.getInterruptStatus();
    interruptManager.disableInterrupt();

    // 分配一页作为PCB
    PCB *thread = allocatePCB();

    if (!thread)
        return -1;

    // 初始化分配的页
    memset(thread, 0, PCB_SIZE);

    for (int i = 0; i < MAX_PROGRAM_NAME && name[i]; ++i)
    {
        thread->name[i] = name[i];
    }

    thread->status = ProgramStatus::READY;
    thread->priority = priority;
    thread->ticks = priority * 10;
    thread->ticksPassedBy = 0;
    thread->pid = ((int)thread - (int)PCB_SET) / PCB_SIZE;

    // 线程栈
    thread->stack = (int *)((int)thread + PCB_SIZE);
    thread->stack -= 7;
    thread->stack[0] = 0;
    thread->stack[1] = 0;
    thread->stack[2] = 0;
    thread->stack[3] = 0;
    thread->stack[4] = (int)function;
    thread->stack[5] = (int)program_exit;
    thread->stack[6] = (int)parameter;

    allPrograms.push_back(&(thread->tagInAllList));
    readyPrograms.push_back(&(thread->tagInGeneralList));

    // 恢复中断
    interruptManager.setInterruptStatus(status);

    return thread->pid;
}
```

### 3.调试gdb查看进程调度

编写四个线程，第一个线程始终不返回；打开gdb进行调试，查看esp寄存器及其对应地址位置包含的值的变化，进而理解进程调度原理：

1.一个新创建的线程被调度执行的过程：

    首先得要初始化PCB

### 4. 编写调度算法：

本次我们实现两个算法：

###### 1.先到先服务算法：

算法原理：先进入等待队列的线程先运行，且不管当前正在运行的线程有多长，都得等其运行结束才能切换下一个等待队列中的线程。

实现比较简单，只需修改setup中的first_thread函数、ProgramManager中的线程调度schedule函数还有interrupt里面的c_time_interrupt_handler()函数

###### 2.优先级调度算法：

算法原理：在等待队列中挑选优先级最高的线程先运行，直到其运行结束，接着从等待队列中挑选优先级最高的线程运行；

实现方法：这个算法主要的困难在于判断队列中的优先级大小，我引入int变量maxpriority来存放当前最高的优先级大小，然后遍历等待队列，当等待队列中有优先级>maxpriority时，将该优先级赋给maxpriority，这样就得到了最大的优先级；然后再次遍历等待序列，直到找到第一个优先级和maxpriority相同的线程，就将该线程pcb指针赋给next指针，然后完成线程调度操作。

包括：硬件或虚拟机配置方法、软件工具与作用、方案的思想、相关原理、程序流程、算法和数据结构、程序关键模块，结合代码与程序中的位置位置进行解释。不得抄袭，否则按作弊处理。

## 实验过程

（此部分需补充完整）

### Assignment1. 可变参数机制及printf函数的实现

编译运行printf函数，最终输出结果如下：

![1714963456279](image/report/1714963456279.png)

说明满足我们的实验设计

### Assignment2. 线程的实现

编写一个进程，创建第一个线程，结果如下：

![1715223969793](image/report/1715223969793.png)

### Assignment3. 了解线程调度

打开gdb调试界面，在初始函数跳转到内核处设置断点

![1715237256237](image/report/1715237256237.png)

然后使用s语句单步进入调用函数

![1715237773876](image/report/1715237773876.png)

接着初始化中断管理器

![1715238244079](image/report/1715238244079.png)

初始化InterruptManager的具体过程: 设置中断描述符之后,初始化8259A芯片

![1715238588365](image/report/1715238588365.png)

接着打开主片时钟中断：

![1715238781809](image/report/1715238781809.png)

设置时钟中断（包括中断描述符）：

![1715238976368](image/report/1715238976368.png)

初始化输出管理器:

![1715239088682](image/report/1715239088682.png)

初始化线程管理器：

![1715239138833](image/report/1715239138833.png)

接着到了我们的重头戏——创建第一个线程：

![1715239441961](image/report/1715239441961.png)

再创建完第一个线程之后，我们查看当前的寄存器状态:

![1715239841842](image/report/1715239841842.png)

接着到调用线程部分，在进入asm_switch_thread函数时寄存器状态如下：

![1715241152577](image/report/1715241152577.png)

可以观察到esp = 0x7db0，因此我们查看这个内存地址中的值：

![1715241959096](image/report/1715241959096.png)

此时为021d40

压栈之后：

![1715244803206](image/report/1715244803206.png)

调度完第一个程序之后：

![1715244975769](image/report/1715244975769.png)

可以看到esp的值发生了变化，变为了0x22d24

查看该地址的值：

![1715245078672](image/report/1715245078672.png)

此时eax中的内容为：

![1715245453964](image/report/1715245453964.png)

由此，我们可以看出，在线程调度过程中主要是由esp的值发生变换，从而在不同线程的地址之间进行切换，具体来说：mov eax,[esp + 5*4] 语句首先将当前栈顶地址加上偏移量计算出地址的值（即cur指针），将其存储到eax中；之后将eax中的值（即当前栈指针）保存到当前线程的PCB中，以便之后可以恢复；mov eax, [esp + 6 *4]语句再将当前栈顶地址加上偏移量计算出地址的值（next指针）存储到eax中；之后mov esp, [eax]将eax中的值（即下一个线程的栈指针）加载到esp中，这一步将栈指针从当前线程切换到了下一个线程，完成线程切换操作。

至于ret，在第一次切换进程时，ret返回的就是function的首地址；如果之后再次进行切换进程时，返回的就是schedule函数里面的return语句，之后接着继续中断操作。

### Assignment4. 实现调度算法

###### 实现1：先到先服务算法

我将我们的进程创建函数中的一个线程的ticks设置为了2*priority，为了体现出我们是先到先服务算法，我们应该将不同线程的ticks设置的不一样，将线程1设置为10，线程2设置为20，线程3设置为30，而且，为了让线程调度更明显，我将三个线程顺序设置为3、2、4，因此我们第一个线程的函数编写如下：

```
void first_thread(void *arg)
{
    // 第1个线程不可以返回
  
    printf("pid %d name \"%s\": Hello 1!\n", programManager.running->pid, programManager.running->name);
    if (!programManager.running->pid)
    {
        programManager.executeThread(third_thread, nullptr, "third thread", 2);
	programManager.executeThread(second_thread, nullptr, "second thread", 1);
        programManager.executeThread(forth_thread, nullptr, "forth thread", 3);
    }
    asm_halt();
}
```

schedule函数我们也改变其代码如下：

![1715327621051](image/report/1715327621051.png)

可以看到我们没有将ticks重新设置值，而是加入了ticks=0的判断，如果ticks=0，则直接将该进程结束，而不是进入等待队列中。

最终运行的结果如下：

![1715331177957](image/report/1715331177957.png)

可以看出该算法是将当前正在运行的线程执行完毕后才切换到下一个进程。

###### 实现算法2：优先级调度算法

修改schedule函数如下:

![1715334788627](image/report/1715334788627.png)

setup函数如下：可以看到是second_thread先创建进入等待队列，forth_thread最后;

优先级forth_thread最高（3），second_thread最低（1）

![1715334624358](image/report/1715334624358.png)

运行结果如下：
![1715334959725](image/report/1715334959725.png)

可以看到优先级最高的forth_thread先执行了，优先级最低的second_thread最后执行，说明成功实现了优先级调度算法。

包括：主要工具安装使用过程及截图结果、程序过程中的操作步骤、测试数据、输入及输出说明、遇到的问题及解决情况、关键功能或操作的截图结果。不得抄袭，否则按作弊处理。

## 实验总结

这次实验我充分理解了操作系统中线程的实现原理和调度算法的编写。首先我分清了进程和线程的区别：进程是指⼀个内存中运⾏的应⽤程序，每个进程都有⼀个独⽴的内存空间，⼀个应⽤程序可以同时运⾏多个进程；进程也是程序的⼀次执⾏过程，是系统运⾏程序的基本单位；系统运⾏⼀个程序即是 ⼀个进程从创建、运⾏到消亡的过程。线程是进程中的⼀个执⾏单元，负责当前进程中程序的执⾏，⼀个进程中⾄少有⼀个线程。⼀个进程中是可以有多个线程的，这个应⽤程序也可以称之为多线程程序。其次，实现调度算法的过程，让我对线程之间的切换和调度有了深刻的理解。我首先实现了一个先到先服务算法（FIFS），只需将时间片轮换算法中对ticks的重新赋值环节取消即可。至于优先级调度算法，只需遍历一边waiting队列中的线程，找出其中优先级最高的进程即可，这里用到了链表的相关知识，对于我的综合能力是一个考验。

总的来说，这次实验的要求比之前都要高上不少，对于我的能力还有学习的掌握情况都有严格要求。我会努力进取，掌握牢靠相关知识，来迎接未来道路上的挑战！

每人必需写一段，文字不少于500字，可以写心得体会、问题讨论与思考、新的设想、感言总结或提出建议等等。不得抄袭，否则按作弊处理。

## 参考文献

（如有要列出，包括网上资源）

[#ifndef是什么意思-CSDN博客](https://blog.csdn.net/m0_55048235/article/details/122427941?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522171496426816800227466578%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=171496426816800227466578&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_ecpm_v1~rank_v31_ecpm-2-122427941-null-null.142^v100^pc_search_result_base1&utm_term=%23ifndef%E6%98%AF%E4%BB%80%E4%B9%88%E6%84%8F%E6%80%9D&spm=1018.2226.3001.4187)
